﻿using System.Text.Json.Serialization;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostEditDTO
    {
        [JsonIgnore]
        public int AuthorId { get; set; }

        public int Id { get; set; }
        public string PreviewImage { get; set; }
        public string Body { get; set; }
    }
}
