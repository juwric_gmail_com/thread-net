export interface NewPost {
    authorId: number;
    id: number | null;
    body: string;
    previewImage: string;
}
